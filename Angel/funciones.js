//para validar que solo acepte numeros

function intnumeros(e) {

    numero = e.numeroCode || e.which;

    teclado = String.fromCharCode(numero);

    aceptados = "0123456789";

    caracteres = "8-37-39-46";

    espacio = false;

    for (var i in caracteres) {
        if (numero == caracteres[i]) {
            espacio = true;
        }
    }

    if (aceptados.indexOf(teclado) == -1 && !espacio) {
        return false;
    }
}



//para asignar cada valor

function asignarvalor(num) {

    var numanterior = document.fo.valores.value;

    document.getElementById("valores").value = numanterior + num;
}


//para eliminar ultimo caracter
function suprimir() {

    var numanterior = document.fo.valores.value;
    var nuevonum = numanterior.substring(0, numanterior.length - 1);
    document.getElementById("valores").value = nuevonum;
}

// para eliminar todo
function borrar() {
    document.fo.valores.value = "";
}